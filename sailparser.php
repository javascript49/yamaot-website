
<html dir="rtl">
<head>
      <meta name="viewport" content="width=device-width, user-scalable=false, initial-scale=1, maximum-scale=1;">
      <meta charset="UTF-8">
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <title>Sail Parser</title>
      <style>
	table{
	  width: 100%;
	  height: 100%;
	}
	#outerParser{
	    overflow-y:scroll;
	    width:100%;
	}
	thead td{
	    width:25%;
	}
	#Original{
	    width:100%;
	}
      </style>
      <script>
	    $('document').ready(function(){
		
		var replacementObj = {
				  delete   : "<span class='delete'  style='background-color:#FF0000'>$&</span>",
				  modifyQ  : "<span class='modifyQ' style='background-color:#3399FF'>$&</span>",
				  modifyA1 : "<span class='modifyA' style='background-color:#00CCFF'>$&</span>",
				  modifyA2 : "<span class='modifyA' style='background-color:#00CCC0'>$&</span>"
			  };
			    
		  $('.editable').height(window.innerHeight*0.7);
		  $('#loggerOuter').height(window.innerHeight*0.2);
				    
		  //$('#Original').on("change keypress keyup",function(){
		  $('#Original').on("change",function(){
		    
			  var parsedText = parseIt(this.value,replacementObj);			  
			  $('#Parser').html(parsedText);
		
		  });
		  
		  $('#autoFix').on("click",function(){	
			  var autoFixText = autoFix($('#Original').val());
			  var parsedText = parseIt(autoFixText,replacementObj);
			  
			  $('#Parser').html(parsedText);	  
		  });
		  
		  $('.editable').on("scroll",function(){
				  var currentScroll = $(this).scrollTop();
				  var scrollHeight = $(this)[0].scrollHeight;
				  var relativeScroll = currentScroll/scrollHeight;
				  
				  if($('#Original').is(":focus"))
					  $('#outerParser').scrollTop($('#Parser').height()*relativeScroll);
				  else
					  $('#Original').scrollTop($('#Original')[0].scrollHeight*relativeScroll);
				  
		  });
	        
	    });
	    
	    function autoFix(value){
		var parsedValue = value;
		parsedValue = parsedValue.replace(/(\n){1}(\s*)(\d+)(\s*)(\.){0,1}(.*)(\?|\:){1}(\n){1}/g,"\n\n$3. $6$7\n") ;
		parsedValue = parsedValue.replace(/(\n){1}(\s*)([א-ת])(\s*)(\.)(\s*)(.*)(\.){1}/g,"\n$3. $7") ;
		parsedValue = parsedValue.replace(/(\n){1}(\s*)([א-ת])(\s*)(\.)(\s*)(.*)(\.){0}/g,"\n$3. $7 .") ;
		return parsedValue;
		/*
		  var parsedValue = value;
		  var questionCounter = 1;
		  var answerCounter = 1;
		  var lettersToNumbers = [ 'א','ב','ג','ד' ];
		  $('#Parser span').each(function() {
			if ($(this).attr('class') == 'delete')
			      $(this).remove();
			else if ($(this).attr('class') == 'modifyQ'){
			      var questionNumber = $(this).text().match(/(\d+)/m) ;
			      if(questionNumber[0] != questionCounter){
				    $('#logger ul').append('<div>error in question number '+questionCounter+'</div>');
				    questionCounter = questionNumber[0] ;
			      }
			      questionCounter++;
			      answerCounter = 1;
			}
			else if ($(this).attr('class') == 'modifyA'){
			      var answerNumber = $(this).text().match(/[א|ב|ג|ד]/m) ;
			      if (answerCounter != (parseInt(jQuery.inArray(answerNumber[0],lettersToNumbers))+1)){
				    $('#logger ul').append('<li>error in question number '+questionCounter+' at answer '+answerNumber[0]+'</li>');
				    answerCounter = parseInt(jQuery.inArray(answerNumber[0],lettersToNumbers))+1;
			      }
			      answerCounter++; 
			}
		  });
		*/
	    }
	    
	    function parseIt(value,replacementObj){
		  var parsedValue = value;
		  parsedValue = parsedValue.replace(/(מדינת ישראל)\s*(([א-תa-zA-Z])+\s*){0,23}(&)\s*(PORTS)/g,replacementObj.delete) ;
		  parsedValue = parsedValue.replace(/(\n)(\d*\s*)(\.){1}(\s+)(.*)(\n*)(.*)(\?|:){1}(.*)/g,replacementObj.modifyQ) ;  
		  parsedValue = parsedValue.replace(/(\n)([א-ד])(\s){0}(\.){1}(.*)(\.){1}/g,replacementObj.modifyA1) ;
		  parsedValue = parsedValue.replace(/\n/g,"<br>") ;
		  parsedValue = parsedValue.replace(/\s/g," ") ;
		    /*
		    parsedValue = parsedValue.replace(/(\n)([\u0591-\u05F4]){1}(\.)+(.*)(\.){1}[^\n]([\u0591-\u05F4]){1}(\.)(.*)[\n]([\u0591-\u05F4]){1}(\.)+(.*)(\.){1}[^\n]([\u0591-\u05F4]){1}(\.)(.*)/g,"$1$2$3$4$5\n$9$10$11$12\n$6$7$8\n$13$14$15") ;
		    parsedValue = parsedValue.replace(/(\n)([\u0591-\u05F4]){1}([\.])+([\u0591-\u05F40-9\s"'!@#$%^&*,?a-zA-Z)(_+=-]*[^\.])(\n)/g,"$1$2$3$4 .$5") ;
		    parsedValue = parsedValue.replace(/(\n)([\u0591-\u05F4]){1}([\.])+([\u0591-\u05F40-9\s"'!@#$%^&*,?a-zA-Z)(_+=-]*)([^\s])([\.])/g,"$1$2$3$4$5 $6") ;	
		    */
		  
		  return parsedValue;
	    }
	    function unicodeConvertor(string){
		  return (escape( string ));
	    }
      </script>
</head>
<body>
<div id="tester"></div>
      <table>
	<thead>
	    <tr>
		  <td><span class="header">Original</span></td>
		  <td><button id="autoFix">autoFix</button></td>
		  <td><input id="startNumber" type="number" /></td>
		  <td><span class="header">Parser</span></td>
	    </tr>
	</thead>
	<tbody>
	    <tr>
		  <td colspan="2">
		    <textarea class="editable" id="Original"></textarea>
		  </td>
		  <td colspan="2">
		    <div class="editable" id="outerParser" ><div  id="Parser" ></div></div>
		  </td>
	    </tr>
	    <tr>
		  <td colspan="4"><div id="loggerOuter"><div  id="logger" ><ul></ul></div></div></td>
	    </tr>
	</tbody>
      </table>
</body>
</html>


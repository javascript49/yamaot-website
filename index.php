<?php header('Content-Type: text/html; charset=utf-8'); ?>
<?php
    if (isset($_REQUEST['comments']) and isset($_REQUEST['solve']) and $_REQUEST['solve'] == 2){
		
	$to  = 'dorc.tech@gmail.com';
	
	// subject
	$subject = 'טופס יצירת קשר מבחני ימאות';
	
	// message
	$message = '
	<html dir="rtl">
	<head>
	  <title>טופס יצירת קשר מבחני ימאות</title>
	</head>
	<body>
	  <p>תוכן ההודעה:</p>
	  <table>
	    <tr><td>שם פרטי:</td><td>'.$_REQUEST["fname"].'</td></tr>
	    <tr><td>שם משפחה</td><td>'.$_REQUEST["lname"].'</td></tr>
	    <tr><td>טלפון:</td><td>'.$_REQUEST["phone"].'</td></tr>
	    <tr><td>אימייל:</td><td>'.$_REQUEST["email"].'</td></tr>
	    <tr><td>סוג מבחן:</td><td>'.$_REQUEST["tesType"].'</td></tr>
	    <tr><td>הערות:</td><td>'.$_REQUEST["comments"].'</td></tr>
	    <tr><td>1+1?:</td><td>'.$_REQUEST["solve"].'</td></tr>
	  </table>
	</body>
	</html>
	';
	
	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	
	// Additional headers
	$headers .= 'To: Dorc <dorc.tech@gmail.com>' . "\r\n";
	$headers .= 'From: yamaot contact form' . "\r\n";
	
	// Mail it
	mail($to, $subject, $message, $headers);
    }
?>
<html dir="rtl">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=false, initial-scale=1, maximum-scale=1;">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <style>
            .answerText{
                cursor:pointer;
            }
            #createWrong{
                display: none;
            }
	   .answerTextOutter{
		display:none;
           }
        </style>
	<title>
	    מקבץ מבחני ימאות
	</title>
	<meta name="description" content="מבחני ימאות ממוחשבים לטובת מעבר מבחני הסקיפר - משיט 11,12,30 ו-60">
	<meta name="keywords" content="מבחני ימאות,מבחני משיט,משיט30,משיט 60,בחינת ימאות,רספן,מבחני רספן,משיט 11,משיט 12">
	<meta name="author" content="Dor Cohen">
    </head>
    <body style="width:95%;overflow-x: hidden;overflow-y: scroll">
	<?php if (isset($_REQUEST['tesType'])){ ?>
        <script>
            var score=0;
            var total=0;
            $(document).ready(function() {
                $('input[type="radio"]').click(function(){
                    if (($(this).parent().attr('solution')!='wrong')&&($(this).parent().attr('solution')!='right')) {
                        total++;
                    }
                    var selectedAnswer = $(this).attr('value');
                    var answer = $(this).parent().attr('answer');
                    if (answer != selectedAnswer) {
                        $(this).next().css('color','red');
                        $(this).parent().attr('solution','wrong');
                        $('#createWrong').show();
                    }
                    else{
                        if (($(this).parent().attr('solution')!='wrong')) {
                            score ++;
                            $(this).parent().attr('solution','right');
                        }
                        $(this).next().css('color','blue');
                    }
                    $('#score').text(score+"/"+total);
                });
                $('.answerText').click(function(){
                    $(this).prev().click();
                });
		 $('.answerText').each(function() {
                    var text = $(this).html();
                    $(this).html(text.replace('שגיאת המצפן', '<a target="_blank" href="http://israelsail.com/attachments/<?php echo $_REQUEST['tesType'] ?>/devTable.png">שגיאת המצפן</a>')); 
                });
                $('.questionText, .answerText').each(function() {
                    var text = $(this).html();
                    $(this).html(text.replace('בתמונה', '<a target="_blank" href="http://israelsail.com/attachments/<?php echo $_REQUEST['tesType'] ?>/sign.pdf">בתמונה</a>')); 
                });
		$('#form_close').click(function(){
                    $("#mailForm").hide();
                });
		$('#form_show').click(function(){
                    $("#mailForm").show();
                });
                $('#createWrong').click(function(){
                    var wrongArray = [];
                    $('p[solution="wrong"]').each(function() {
                        wrongArray.push($(this).attr('question'));
                    });
                    var errorJson=JSON.stringify(wrongArray);
                    $('#wrongTest').val(errorJson);
                    $('#createWrongTest').submit();
                });
                <?php if (isset($_REQUEST['wrongTest'])){ ?>
                    $(".fullQ").hide();
                    var wrongJson = '<?php echo $_REQUEST['wrongTest'] ?>';

                    var wrongArray = JSON.parse(wrongJson);
                    for(var i=0;i<wrongArray.length;i++){
                       $(".fullQ[question='"+wrongArray[i]+"']").show(); 
                    }
                    $(".fullQ:hidden").hide();
                <?php } ?>
            });
        </script>
	<script>
	    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  
	    ga('create', 'UA-45411635-1', 'holonmotor.com');
	    ga('send', 'pageview');
	</script>
        <form id="createWrongTest" style="display: none" method="post">
            <input type="hidden" id="wrongTest"  name="wrongTest" />
            <input type="hidden" name="tesType" value="<?php echo $_REQUEST['tesType'] ?>" />
        </form>
        <span style="position: fixed;left:0px;top:0px;background-color: yellow;">ניקוד : <span id="score">0</span></span>
        <span style="position: fixed;right:0px;bottom:0px;background-color: red;cursor:pointer" id="createWrong"> צור מבחן משאלות בהן טעיתי </span>
	<span id="form_show" style="position: fixed;left:0px;bottom:0px;background-color: red;cursor:pointer"> כתבו לי </span>
	<div id="mailForm" style="width: 100%;height: 100%;position: fixed;background: white;display: none;left:0px;top:0px;">
	    <div>
		אנא כתבו לי בנושאים הבאים:
		<ul>
		    <li>מצאתם טעות באחת השאלות - נא לציין מספר שאלה</li>
		    <li>יש לכם הצעות לשיפור ויעול</li>
		    <li>רוצים לתת פידבק חיובי</li>
		</ul>
	    </div>
	    <div>
		<form method="post">
		    <table>
			<tr><td>שם פרטי:</td><td><input type="text" name="fname" id="form_fname"  /></td>
			<tr><td>שם משפחה:</td><td><input type="text" name="lname" id="form_lname" /></li></td>
			<tr><td>דוא"ל:   </td><td><input type="email" name="email" id="form_email" required /></li></td>
			<tr><td>טלפון:   </td><td><input type="tel" name="phone" id="form_phone" /></li></td>
			<tr style="display:none"><td><input type="text" name="tesType" id="form_test" value="<?php echo $_REQUEST['tesType'] ?>"/></li></td>
			<tr><td>הערות:</td><td><textarea name="comments" id="form_comments" required></textarea></li></td>
			<tr><td>1+1?:</td><td><input type="number" name="solve" id="solve"  /></td>
			<tr><td></td><td><button type="submit" id="form_submit" value="שלח">שלח</button><button type="button" id="form_close" value="סגור">סגור</button></td></tr>
		    </table>
		</form>
	    </div>
	    
	</div>
    <?php
        $data =  file_get_contents("./text/".$_REQUEST['tesType']."/questions.txt");
        $questions = preg_split("/([\n]+[\d]+[.])/i", $data,null,PREG_SPLIT_NO_EMPTY );
        
        $questionNum = 1;
        
        foreach ($questions as $key=>$fullQuestion){
            if ($key>0){
                $question = explode("א.",$fullQuestion);
		if (preg_match("/[&][*][*][*][&]/u", $fullQuestion)){
			$question[0] = preg_replace("/[&***&]/"," ",$question[0]);
			$answers = preg_split("/([\s][א|ב|ג|ד|ה|ו|ז|ח|ט|י|כ|ל|מ|נ|ס|ע|פ|צ|ק]+[.])/iu", $question[1],null,PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE );
				
			echo "<p class='fullQ' question='".$questionNum."'><span class='questionText'>".$questionNum.".".$question[0]."</span><br>";
			if (file_exists("./images/".$_REQUEST['tesType']."/q".$questionNum.".PNG")){
			    echo "<img src='./images/".$_REQUEST['tesType']."/q".$questionNum.".PNG' style='max-width:95%'>";
			}
			echo "<br>";

			echo "<span class='answerText' > א. ".$answers[0]."</span><br><textarea final='null' freeTextId='".$questionNum.".א.'  style='width:100%' name='q".preg_replace('/\s+/', '',$questionNum)."' type='text' value='א.'></textarea><br>";
                        foreach ($answers as $answerKey=>$answerText){
                            if (($answerKey>0)&&($answerKey%2!=0))
                                echo "<span class='answerText' >".$answers[$answerKey]."".$answers[$answerKey+1]."</span><br><textarea final='null' freeTextId='".preg_replace('/\s+/', '',$questionNum).".".preg_replace('/\s+/', '',$answers[$answerKey])."' style='width:100%' name='q".$questionNum."' type='text' value='".preg_replace('/\s+/', '',$answers[$answerKey])."'></textarea><br>";
                        }

			echo "<br><br></p>";
			$questionNum++;
		}
		else {
		        $answers = preg_split("/([\s][א|ב|ג|ד]+[.])/iu", $question[1],null,PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE );
		        
		        echo "<p class='fullQ' question='".$questionNum."'><span class='questionText'>".$questionNum.".".$question[0]."</span><br>";
		        if (file_exists("./images/".$_REQUEST['tesType']."/q".$questionNum.".PNG")){
		            echo "<img src='./images/".$_REQUEST['tesType']."/q".$questionNum.".PNG' style='max-width:95%'>";
		        }
		        echo "<br>";
		        echo "<input name='q".$questionNum."' type='radio' value='א.'><span class='answerText'> א. ".$answers[0]."</span><br>";
		        if(preg_match("/ב./u", $answers[1])){
		            echo "<input name='q".$questionNum."' type='radio' value='".preg_replace('/\s+/', '',$answers[1])."' /><span class='answerText'>".$answers[1]."".$answers[2]."</span><br>";
		            echo "<input name='q".$questionNum."' type='radio' value='".preg_replace('/\s+/', '',$answers[3])."' /><span class='answerText'>".$answers[3]."".$answers[4]."</span><br>";
		        }
		        else{
		            echo "<input name='q".$questionNum."' type='radio' value='".preg_replace('/\s+/', '',$answers[3])."' /><span class='answerText'>".$answers[3]."".$answers[4]."</span><br>";
		            echo "<input name='q".$questionNum."' type='radio' value='".preg_replace('/\s+/', '',$answers[1])."' /><span class='answerText'>".$answers[1]."".$answers[2]."</span><br>";
		        }
		        echo "<input name='q".$questionNum."' type='radio' value='".preg_replace('/\s+/', '',$answers[5])."'><span class='answerText'>".$answers[5]."".$answers[6]."</span><br>";
		        echo "<br><br></p>";
		        $questionNum++;
		    }
		}
        }
        
        $data2 =  file_get_contents("./text/".$_REQUEST['tesType']."/answers.txt");
        $rightAnswers = $question = explode("\n",$data2);
        if (preg_match("/[&***&]/u", $rightAnswers[0])){
            $rightAnswerLine = preg_split("/([\n]+[\d]+[.])/i", $data2,null,PREG_SPLIT_NO_EMPTY );
            
            foreach ($rightAnswerLine as $answerKey=>$answerText){
                if ($answerKey>0){
                    echo "<span class='answerTextOutter'>".$answerKey.".<br>";
                    $rightAnswersDiv = preg_split("/([\s][א|ב|ג|ד|ה|ו|ז|ח|ט|י|כ|ל|מ|נ|ס|ע|פ|צ|ק]+[.])/iu", $answerText,null,PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE );
                    foreach ($rightAnswersDiv as $answerKeyDiv=>$answerTextDiv){
                        if ($answerKeyDiv%2==0)
                            echo "<span class='answerTextInner' answerId='".preg_replace('/\s+/', '',$answerKey).".".preg_replace('/\s+/', '',$rightAnswersDiv[$answerKeyDiv])."'>".preg_replace('/\s+/', '',$rightAnswersDiv[$answerKeyDiv])." ".$rightAnswersDiv[$answerKeyDiv+1]."</span>";
                    }
                    echo "</span>";
                }
            }
            ?>
            
                <script>
                    $('textarea').keypress(function(e) {
			if (e.keyCode == 13){
                            checkFreeTextAnswer($(this));
			    $(this).blur();
			}
                    });
		    $('textarea').blur(function() {
                        checkFreeTextAnswer($(this));
                    });

		    function checkFreeTextAnswer(obj){
		    	if (obj.attr('final') == 'null') {
                    
                            var freetextid = obj.attr('freetextid');
                            var givenAnswer = obj.val();
                            var answerById = $('span[answerid="'+freetextid+'"]').text();
                            
                            var arrayOfRightWords = givenAnswer.split(' ');
                            var arrayOfRightAnswer = answerById.split(' ');
                            var rightWordsCounter = 0;
                            
                            for(var i=0;i<arrayOfRightWords.length;i++){
                                var contains = (answerById.indexOf(arrayOfRightWords[i]) > -1); //true
                                if (contains == true)
                                    rightWordsCounter ++;
                            }
                            
                            var totalPre = 60 ; //60% right answers
                            if (((rightWordsCounter)/(arrayOfRightAnswer.length))*100 >= totalPre){
                                obj.attr('final','right');
                                answerColor = "blue";
                                score++;
                            }
                            else{
                                obj.attr('final','wrong');
                                answerColor = "red";
                            }
                                    
                            obj.after("<div style='color:"+answerColor+"'>"+answerById+"</div>");
                            total++;
                            $('#score').text(score+"/"+total);
                        }
		    }
                </script>
            
            <?php
        }
            
        else{
            foreach ($rightAnswers as $key=>$rightAnswerLine){
                $rightAnswersArray = preg_split("/([\d]+[ ]+[א|ב|ג|ד])/iu", $rightAnswerLine,null,PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE );
                foreach($rightAnswersArray as $key=>$type){
                    $finalAnswer = explode(" ",$type);
                    if (isset($finalAnswer[0])&&isset($finalAnswer[1])&&!empty($finalAnswer[0])&&!empty($finalAnswer[1])){
                        echo "<script>$('p[question=\"".$finalAnswer[0]."\"]').attr('answer','".$finalAnswer[1].".')</script>";
                        //echo $finalAnswer[0]."---".$finalAnswer[1]."<br>";
                    }
                }
            }
        }
    ?>
    <?php } else{ ?>
	<div>
	    <h2 style="text-decoration: underline;">
		מקבץ מבחני ימאות
	    </h2>
	    <p>
		המבחנים באתר זה <b>לא נכתבו על ידי</b> אלא נקראים באופן <b>אוטומטי לחלוטין</b> ממאגר השאלות של רספ"ן ולכן הקרדיט לשאלות שמור להם.
	    </p>
	    <p>
		כל יתרון המערכת הנ"ל הוא בהצגת השאלות בצורה ממוחשבת ונוחה למענה עם תשובות מיידיות ושמירת ניקוד.
	    </p>
	    <p>
		ייתכנו באגים בשאלות מסוימות בגלל שהמבחן נבנה בצורה אוטומטית ותלוי על כן ברספ"ן, לכן אם נמצאה טעות בשאלה אנא כתבו לי בטופס יצירת הקשר ואפעל לתקנה.
	    </p>
	    <p>
		המערכת נבנתה בעזר לימאים עתידיים לעבור בהצלחה את מבחני רספ"ן ואינה בתשלום, <b>המערכת חינמית לחלוטין</b>.
	    </p>
	    <p>
		כרגע המערכת מכילה את מבחני הימאות למשיט 30 ,מכונות ,ניווט ב' מכשירים ומבחן הניווט א' בעתיד אוסיף מבחני ימאות לדרגות משיט נוספות.
	    </p>
	    <p>
		אם ברצונכם שאוסיף עוד מבחנים למאגר אנא שלחו לי הודעה בטופס יצירת הקשר.
	    </p>
		<p>
			<b><font color="red">עידכון 12.3.2017 - תוקנו מספר בעיות כולל אופציית "צור מבחן משאלות שטעיתי"</font></b>
		</p>
	</div>
	<div>אנא בחר מבחן</div>
	<ul>
            <li><a href="./index.php?tesType=jetski11">אופנוע ים(11)</a></li>
            <li><a href="./index.php?tesType=fastboat12">סירה מהירה(12)</a></li>
	    <li><a href="./index.php?tesType=yamaot30">ימאות(30)</a></li>
	    <li><a href="./index.php?tesType=mechonot30">מכונות(30)</a></li>
	    <li><a href="./index.php?tesType=nevotA30">ניווט א' חופי(30)</a> </li>
	    <li><a href="./index.php?tesType=nevotB30">ניווט ב' מכשירים(30)</a></li>
	    <li><a href="./index.php?tesType=gmdss">gmdss רדיו(60)</a> - <font color="red">טרם נבדק יתכנו תשובות שגויות</font></li>
	</ul>
    <?php } ?>
    </body>
</html>
